import React from "react"
import { Route, Routes } from "react-router-dom"
import Albums from "../../components/pages/albums"
import Favs from "../../components/pages/favs"
import Lists from "../../components/pages/lists"
import Login from "../../components/pages/login"

const Routing = () => {
  return (
    <Routes>
      <Route path="/" element={<Login />} />
      <Route path="/home" element={<Lists />} />
      <Route path="/favs" element={<Favs />} />
      <Route path="/albums" element={<Albums />} />
    </Routes>
  )
}

export default Routing
