import React, { useEffect, useState } from "react"
import { services } from "../../services/services"
import ListTemplate from "../templates/list-template/list-template"

const Lists = () => {
  const [recent, setRecent] = useState(null)
  const [favs, setFavs] = useState(null)

  useEffect(() => {
    services.getData(setRecent, setFavs)
  }, [])

  return (
    <ListTemplate
      songs={recent}
      favs={favs}
      title="Home"
      subtitle="Your recently played songs"
    />
  )
}

export default Lists
