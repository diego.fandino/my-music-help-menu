import React, { useEffect, useState } from "react"
import { services } from "../../services/services"
import ListTemplate from "../templates/list-template/list-template"

const Albums = () => {
  const [albums, setAlbums] = useState(null)

  useEffect(() => {
    services.getAlbums(setAlbums)
  }, [])

  return (
    <ListTemplate
      songs={albums}
      title="Albums"
      subtitle="Your playlists"
      isAlbum={true}
    />
  )
}

export default Albums
