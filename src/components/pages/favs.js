import React, { useEffect, useState } from "react"
import { services } from "../../services/services"

import ListTemplate from "../templates/list-template/list-template"

const Favs = () => {
  const [favs, setFavs] = useState(null)

  useEffect(() => {
    services.getFavs(setFavs)
  }, [])

  return (
    <ListTemplate songs={favs} title="Favs" subtitle="Your favorite songs" />
  )
}

export default Favs
