import React from "react"
import "./song-card.scss"
import { services } from "../../../services/services"

import { ReactComponent as HeartRegularIcon } from "../../../assets/Images/heart-regular.svg"
import { ReactComponent as HeartSolidIcon } from "../../../assets/Images/heart-solid.svg"

const SongCard = props => {
  const { song, index, title, favs, setActualPlayer } = props
  const playSong = id => {
    setActualPlayer(`track/${id}`)
    localStorage.setItem("actual", `track/${id}`)
  }

  return (
    <div className="item">
      <span onClick={() => playSong(song.id)} className="item__number">
        {index + 1}
      </span>
      <div className="item__container">
        <img
          src={song.album.images[0].url}
          className="item__imag"
          alt="Music logo"
        />
        <div className="item__title">
          <b>{song.name}</b>
          <span>{song.artists[0].name}</span>
        </div>
      </div>

      {title === "Home" ? (
        favs && favs[index] ? (
          <HeartSolidIcon
            className="item__heart-icon"
            onClick={() => services.deleteFavorite(song.id)}
          />
        ) : (
          <HeartRegularIcon
            className="item__heart-icon"
            onClick={() => services.putFavorite(song.id)}
          />
        )
      ) : (
        <HeartSolidIcon
          className="item__heart-icon"
          onClick={() => services.deleteFavorite(song.id)}
        />
      )}
    </div>
  )
}

export default SongCard
