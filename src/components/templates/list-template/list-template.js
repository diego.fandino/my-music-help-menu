import React, { useState } from "react"
import Header from "../../molecules/header/header"
import Menu from "../../molecules/menu/menu"
import ItemsGrid from "../../organisms/items-grid/items-grid"
import "./list-template.scss"

const ListTemplate = props => {
  const { title, subtitle, songs, isAlbum, favs } = props
  const [isMenuClosed, setIsMenuClosed] = useState(true)
  const [actualPlayer, setActualPlayer] = useState(
    localStorage.getItem("actual")
  )

  return (
    <>
      <Menu
        isMenuClosed={isMenuClosed}
        setIsMenuClosed={setIsMenuClosed}
        className="menu"
      />
      <Header
        setIsMenuClosed={setIsMenuClosed}
        title={title}
        subtitle={subtitle}
      />
      <ItemsGrid
        songs={songs}
        isAlbum={isAlbum}
        favs={favs}
        title={title}
        setActualPlayer={setActualPlayer}
        className="grid"
      />

      <footer className="footer">
        <iframe
          title="Music player"
          src={`https://open.spotify.com/embed/${actualPlayer}`}
          width="100%"
          height="80px"
          frameBorder="0"
          allowtransparency="true"
          allow="encrypted-media"
        ></iframe>
      </footer>
    </>
  )
}

export default ListTemplate
