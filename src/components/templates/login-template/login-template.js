import React from "react"
import Title from "../../atoms/title/title"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faSpotify } from "@fortawesome/free-brands-svg-icons"
import "./login-template.scss"
import constants from "../../../constants/constants"

const LoginTemplate = () => {
  return (
    <div className="container">
      <Title />
      <a className="container__button" href={constants.SPOTIFY_REDIRECT}>
        <FontAwesomeIcon className="container__icon" icon={faSpotify} />
        Conectarse con Spotify
      </a>
    </div>
  )
}

export default LoginTemplate
